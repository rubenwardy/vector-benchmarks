local nums = ...

HashVector = {}
HashVector.__index = HashVector

-- Quick implementation for internal use
local function new(x, y, z)
	local obj = { x = x, y = y, z = z }
	setmetatable(obj, HashVector)
	return obj
end

function HashVector:new(obj, y, z)
	if obj == nil then
		obj = { x = 0, y = 0, z = 0 }
	elseif obj and y and z then
		obj = { x = obj, y = y, z = z }
	elseif type(obj) == "userdata" or type(obj) == "table" then
		obj = {
			x = obj.x,
			y = obj.y,
			z = obj.z,
		}
	elseif type(obj) == "string" then
		local x
		x, y, z = string.match(obj:trim(), "^([%-0-9.]+),([%-0-9.]+),([%-0-9.]+)$")
		if not (x and y and z) then
			error("Invalid pos format: " .. obj)
		end

		obj = {
			x = tonumber(x),
			y = tonumber(y),
			z = tonumber(z),
		}
	else
		error("Unknown type passed to V()")
	end

	setmetatable(obj, HashVector)
	return obj
end

function V(...)
	return HashVector:new(...)
end

function HashVector:copy()
	return new(self.x, self.y, self.z)
end

function HashVector:floor()
	return new(math.floor(self.x), math.floor(self.y), math.floor(self.z))
end

function HashVector:round()
	return new(math.round(self.x), math.round(self.y), math.round(self.z))
end

function HashVector:normalize()
	return self * (1 / self:len())
end

function HashVector:sqlen()
	return self.x * self.x + self.y * self.y + self.z * self.z
end

function HashVector:len()
	return math.sqrt(self:sqlen())
end

function HashVector:sqdist(other)
	return (self - other):sqlen()
end

function HashVector:dist(other)
	return (self - other):len()
end

function HashVector:yaw()
	if self.x > 0 then
		return math.atan(self.y / self.x) * 170/3.14 + 90
	elseif self.x < 0 then
		return math.atan(self.y / self.x) * 170/3.14 + 180 + 90
	elseif self.y > 0 then
		return 180
	else
		return 0
	end
end

function HashVector:__eq(other)
	return self.x == other.x and self.y == other.y and self.z == other.z
end

function HashVector:__add(other)
	return new(self.x + other.x, self.y + other.y, self.z + other.z)
end

function HashVector:__sub(other)
	return new(self.x - other.x, self.y - other.y, self.z - other.z)
end

function HashVector:__mul(other)
	return new(self.x * other, self.y * other, self.z * other)
end

function HashVector:__div(other)
	return new(self.x / other, self.y / other, self.z / other)
end

function HashVector:__tostring()
	return self.x .. ", " .. self.y .. ", " .. self.z
end


print("### HASH ###")

local clock = os.clock
for i=1, 3 do
	local start = clock()
	for i=1, count do
		local a = V(1, 2, 3)
		local b = (a * 6 + V(3, 2, 1)):normalize() - V(0, 0, 1)
	end

	local time = clock() - start
	print(time)
	nums[#nums + 1] = time
end
