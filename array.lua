local nums = ...

ArrayVector = {}
ArrayVector.__index = ArrayVector

-- Quick implementation for internal use
local function new(x, y, z)
	local obj = { x, y, z }
	setmetatable(obj, ArrayVector)
	return obj
end

function ArrayVector:new(obj, y, z)
	if obj == nil then
		obj = { 0, 0, 0 }
	elseif obj and y and z then
		obj = { obj, y, z }
	elseif type(obj) == "userdata" or type(obj) == "table" then
		obj = {
			obj[1],
			obj[2],
			obj[3],
		}
	elseif type(obj) == "string" then
		local x
		x, y, z = string.match(obj:trim(), "^([%-0-9.]+),([%-0-9.]+),([%-0-9.]+)$")
		if not (x and y and z) then
			error("Invalid pos format: " .. obj)
		end

		obj = {
			tonumber(x),
			tonumber(y),
			tonumber(z),
		}
	else
		error("Unknown type passed to V()")
	end

	setmetatable(obj, ArrayVector)
	return obj
end

function V(...)
	return ArrayVector:new(...)
end

function ArrayVector:copy()
	return new(self[1], self[2], self[3])
end

function ArrayVector:floor()
	return new(math.floor(self[1]), math.floor(self[2]), math.floor(self[3]))
end

function ArrayVector:round()
	return new(math.round(self[1]), math.round(self[2]), math.round(self[3]))
end

function ArrayVector:normalize()
	return self * (1 / self:len())
end

function ArrayVector:sqlen()
	return self[1] * self[1] + self[2] * self[2] + self[3] * self[3]
end

function ArrayVector:len()
	return math.sqrt(self:sqlen())
end

function ArrayVector:sqdist(other)
	return (self - other):sqlen()
end

function ArrayVector:dist(other)
	return (self - other):len()
end

function ArrayVector:yaw()
	if self[1] > 0 then
		return math.atan(self[2] / self[1]) * 170/3.14 + 90
	elseif self[1] < 0 then
		return math.atan(self[2] / self[1]) * 170/3.14 + 180 + 90
	elseif self[2] > 0 then
		return 180
	else
		return 0
	end
end

function ArrayVector:__eq(other)
	return self[1] == other[1] and self[2] == other[2] and self[3] == other[3]
end

function ArrayVector:__add(other)
	return new(self[1] + other[1], self[2] + other[2], self[3] + other[3])
end

function ArrayVector:__sub(other)
	return new(self[1] - other[1], self[2] - other[2], self[3] - other[3])
end

function ArrayVector:__mul(other)
	return new(self[1] * other, self[2] * other, self[3] * other)
end

function ArrayVector:__div(other)
	return new(self[1] / other, self[2] / other, self[3] / other)
end

function ArrayVector:__tostring()
	return self[1] .. ", " .. self[2] .. ", " .. self[3]
end

print("### ARRAY ###")

local clock = os.clock
for i=1, 3 do
	local start = clock()
	for i=1, count do
		local a = V(1, 2, 3)
		local b = (a * 6 + V(3, 2, 1)):normalize() - V(0, 0, 1)
	end

	local time = clock() - start
	print(time)
	nums[#nums + 1] = time
end
