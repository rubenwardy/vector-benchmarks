if jit then
	count = 1000000000
else
	count = 100000
end

local array = {}
local hash = {}

local function run(file, nums)
	loadfile(file)(nums)
end

-- Don't count first run
run("array.lua", {})
run("hash.lua", {})

-- Now benchmark
run("array.lua", array)
run("hash.lua", hash)
run("array.lua", array)
run("hash.lua", hash)

local function avg(tab)
	local count = 0
	for i=1, #tab do
		count = count + tab[i]
	end
	return math.floor(100 * count / #tab) / 100
end


print("===========")
print("Arrays: " .. avg(array))
print("Hash: " .. avg(hash))
